FROM docker.io/alpine:latest

RUN apk add --no-cache openjdk17-jre-headless curl

RUN curl -sSLf "$(curl -sSLf https://api.github.com/repos/Endava/cats/releases/latest | grep browser_download_url | grep cats_uberjar | cut -d\" -f 4)" -o cats.tar.gz && tar xf cats.tar.gz && rm cats.tar.gz

ENTRYPOINT ["java", "-jar", "/cats.jar"]

CMD ["help"]
